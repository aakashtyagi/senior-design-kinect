from django.db import models
from django.contrib.auth.models import User
from django.forms import ModelForm
from django.contrib.auth.models import AbstractBaseUser

# Create your models here.
class AppAdmin(models.Model):
	user = models.OneToOneField(User)
	username = models.CharField(max_length=50, unique=True, default="")
	password = models.CharField(max_length=50, null=False, default="")
	firstname = models.CharField(max_length=50, null=False, default="")
	lastname = models.CharField(max_length=50, null=False, default="")
	# workplace = models.CharField(max_length=50,null=False,default="")

	def __unicode__(self):  #For Python 2, use __str__ on Python 3
		return self.username

class Therapist(models.Model):
	user = models.OneToOneField(User)
	username = models.CharField(max_length=50, unique=True, default="")
	password = models.CharField(max_length=50, null=False, default="")
	firstname = models.CharField(max_length=50, null=False, default="")
	lastname = models.CharField(max_length=50, null=False, default="")
	workplace = models.CharField(max_length=50,null=False,default="")

	def __unicode__(self):  #For Python 2, use __str__ on Python 3
		return self.username


class Patient(models.Model):
	# id = models.AutoField(primary_key=True)
	therapistId = models.ForeignKey('Therapist')
	username = models.CharField(max_length=50, default="")
	password = models.CharField(max_length=50, null=False, default="")
	start_date = models.DateField(auto_now = False, auto_now_add = False)
	end_date = models.DateField(auto_now = False, auto_now_add = False)

	def __unicode__(self):
		return self.username

class Exercise(models.Model):
	# id = models.AutoField(primary_key=True)
	exerciseName = models.CharField(max_length=50)
	category = models.CharField(max_length=50)
	desc = models.CharField(max_length=350)
	date = models.DateField(auto_now=True)
	gbd_name = models.CharField(max_length=50, help_text='Enter your gbd filename. For example, if your file is named: "shoulder-touch.gbd", you must enter "shoulder-touch.gbd"')
	added_by = models.ForeignKey('Therapist')
	# docfile = models.FileField(upload_to='gbd/')
	#exerciseData. We need to figure out how we are going to store this.

	def __unicode__(self):
		return self.exerciseName

class Session(models.Model):

	def number():
		no = Session.objects.count()
		if no == None:
			return 1
		else:
			return no + 1

	ordering = models.IntegerField(max_length=6, unique=True, default=number)
	sessdate = models.DateField()
	exercise = models.ForeignKey('Exercise')
	reps = models.IntegerField()
	time = models.IntegerField() #expected_time
	completed_time = models.IntegerField()
	finished = models.IntegerField()
	user = models.ForeignKey('Patient')

	class Meta:
		unique_together = ("ordering","sessdate")

	def __unicode__(self):
		return str(self.user)

class SessionExercises(models.Model):
	sessionId = models.ForeignKey('Session')
	exerciseId = models.ForeignKey('Exercise')

#
## exercises 
class Document(models.Model):
	docfile = models.FileField(upload_to='gbd/')

class Results(models.Model):
	ordering = models.IntegerField()
	date = models.DateField(auto_now = False, auto_now_add = False)
	total_reps = models.IntegerField()
	completed_reps = models.IntegerField()
	completed_time = models.IntegerField()
	expected_time = models.IntegerField() 
	patient = models.ForeignKey('Patient')
	exercise = models.ForeignKey('Exercise')

	def __unicode__(self):
		return unicode(self.date)

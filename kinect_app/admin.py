from django.contrib import admin
from .models import Therapist, Patient, Exercise, Session, SessionExercises, Results
# Register your models here.
class TherapistAdmin(admin.ModelAdmin):
    fields = ['username', 'password']

admin.site.register(Therapist, TherapistAdmin)
admin.site.register(Patient)
admin.site.register(Exercise)
admin.site.register(Session)
admin.site.register(SessionExercises)
admin.site.register(Results)
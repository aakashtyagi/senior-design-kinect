from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth import models
from django.contrib.auth.models import Group
from .models import Therapist, Patient, Exercise, Document, Results, Session
from .forms import TherapistForm, PatientForm, AdminForm, ExerciseForm, DocumentForm, AssignSessionForm, NewAssignSessionForm
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User, Group
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
# Create your views here.
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.core.urlresolvers import reverse
import datetime
from django.contrib import messages 
import os
from chartit import DataPool, Chart
import simplejson
from django.utils.safestring import mark_safe


def check_patient(user):
    group = models.Group.objects.get(name="Therapist")
    therapist_list = group.user_set.all()
    if user in therapist_list:
        return True
    else:
        return False

def index(request):
    group = models.Group.objects.get(name="Therapist")
    therapist_list = group.user_set.all()
    context_dict = {'therapists' : therapist_list}

    return render(request, 'index.html', context_dict)

@user_passes_test(check_patient,redirect_field_name=None)
@login_required
def create_new_patient_account(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = PatientForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            all_patients = Patient.objects.all()
            names = []
            for p in all_patients:
                names.append(p.username)
            if username in names:
                messages.error(request, 'This username is taken.')
                return render(request, 'new_patient_account.html', {'form': form})

            if password != form.cleaned_data['confirm']:
                messages.error(request, 'Your passwords do not match.')
                return render(request, 'new_patient_account.html', {'form': form})

            if form.cleaned_data['end_date'] <= form.cleaned_data['start_date']:
                messages.error(request, 'Completion date must come after start date.')
                return render(request, 'new_patient_account.html', {'form': form})

            user = User.objects.create_user(username=username, password=password)
            
            newgroup = Group.objects.get_or_create(name='Patient')
            g = Group.objects.get(name='Patient') 
            g.user_set.add(user)
            user.save()
            
            
            f = form.save(commit=False)
            u = request.user
            therapist = Therapist.objects.get(username = u.username)
            f.therapistId = therapist
            f.save()
            return HttpResponseRedirect('/patient-list/')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = PatientForm()

    return render(request, 'new_patient_account.html', {'form': form})


@login_required
def create_new_therapist_account(request):
    # if this is a POST request we need to process the form data
    # user = request.user
    group = Group.objects.get_or_create(name='Therapist')
    user = request.user
    thera = 0
    # group = models.Group.objects.get(name="Therapist")
    try:
        therapist_list = group.user_set.all()
        if user in therapist_list:
            thera = 1
        else:
            thera = 0
    except Exception, e:
        pass
        
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = TherapistForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            

            user = User.objects.create_user(username=username, password=password)
            user.is_staff = True
            newgroup = Group.objects.get_or_create(name='Therapist')
            g = Group.objects.get(name='Therapist') 
            g.user_set.add(user)
            firstname = form.cleaned_data['firstname']
            lastname = form.cleaned_data['lastname']
            workplace = form.cleaned_data['workplace']
            print workplace
            user.first_name = firstname
            user.last_name = lastname
            user.save()
            therapist = Therapist(user=user,username=username,password=password,firstname=firstname,lastname=lastname,workplace=workplace)
            therapist.save()
            return HttpResponseRedirect('/kinect-app/welcome/')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = TherapistForm()

    return render(request, 'new_account.html', {'form': form, 'thera' : thera})


@login_required
def create_new_admin_account(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = AdminForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            users = User.objects.filter(is_superuser=True)
            names=[]
            for i in users:
                names.append(i.username)
            if username in names:
                messages.error(request, 'This username is taken.')
                return render(request, 'new_admin_account.html', {'form': form})

            user = User.objects.create_user(username=username, password=password)
            user.is_superuser = True
            user.is_staff = True
            # newgroup = Group.objects.get_or_create(name='Therapist')
            # g = Group.objects.get(name='Therapist') 
            # g.user_set.add(user)
            firstname = form.cleaned_data['firstname']
            lastname = form.cleaned_data['lastname']
            user.first_name = firstname
            user.last_name = lastname
            user.save()
            return HttpResponseRedirect('/kinect-app/welcome/')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = AdminForm()

    return render(request, 'new_admin_account.html', {'form': form})


def user_login(request):
    if request.user.is_authenticated():
        return welcome_user(request)
    else:

        if request.method == 'POST':
            username = request.POST['username']
            password = request.POST['password']
            # success_url = "index.html"
            user = authenticate(username=username, password=password)
            if user:
                if user.is_active:
                    login(request, user)
                    # Redirect to a success page.
                    # print "here"
                    return HttpResponseRedirect('/kinect-app/welcome/')
                else:
                    return HttpResponse("Your Elemental Kinection account is disabled.")

            else:
                print "Invalid login details: {0}, {1}".format(username, password)
                # return HttpResponse("Invalid login details supplied.")
                invalid = 1
                return render(request, 'login.html', {'invalid':invalid,})

        else:
            return render(request, 'login.html', {})

def welcome_user(request):
    user = request.user
    name = user.username
    group = models.Group.objects.get_or_create(name="Therapist")
    try:
        group = models.Group.objects.get(name="Therapist")
        therapist_list = group.user_set.all()
        if user in therapist_list:
            context_dict = {'user' : user}
            # return render(request, 'patient_list.html', context_dict)
            return view_patient(request)
    except Exception, e:
        pass
    
    if user.is_superuser == True:
        context_dict = {'user' : user}
        return render(request, 'welcome_user.html', context_dict)
    # elif user in therapist_list:
    #     context_dict = {'user' : user}
    #     return render(request, 'welcome_therapist.html', context_dict)
    else:
        try:
            group = models.Group.objects.get(name="Patient")
            patient_list = group.user_set.all()
            if user in patient_list:
                return for_patient_stats(request, name)
        except:
            pass


@login_required
def for_patient_stats(request, anystring=None):
    if anystring:
            # user = request.user
            # therapist = Therapist.objects.filter(username=user.username)
            # group = models.Group.objects.get(name="Therapist")
            # users = group.user_set.all()
            
            patient = Patient.objects.get(username=anystring)
            results = Results.objects.filter(patient=patient)
            total_reps = []
            total = []
            completed_reps = []
            completed = []
            dates = []
            for result in results:
                dates.append(result.date)
                total_reps.append(result.total_reps)
                completed_reps.append(result.completed_reps)

            date = []
            month = []
            for d in dates:
                date.append(d.day)
                month.append(d.month)

            # for t in total_reps:
            #     total.append(int(t))

            # for c in completed_reps:
            #     completed.append(int(c))

            exercises = []
            exercise = []
            for i in results:
                exercises.append(i.exercise)

            exercise = list(set(exercises))

            i_am_patient = 1

            # print total, completed, date
            context_dict = {'results' : results, 'patient' : patient, 'exercise' : exercise,'i_am_patient':i_am_patient,}
            return render(request, "results.html", context_dict)


@login_required
def for_patient_exercise_stats(request, anynumber=None, anystring=None):
    if anynumber:
        if anystring:
            
                patient = Patient.objects.get(id = anynumber)
                exercise = Exercise.objects.filter(exerciseName = anystring)
                results = Results.objects.filter(patient=patient, exercise=exercise)
                yes = 1
                context_dict = {}
                label = str(anystring) + " Results"
                graph_data = Results.objects.filter(patient=patient, exercise=exercise)
                print graph_data[0].date
                print str(graph_data[0].date)
                for i in graph_data:
                    i.date = str(i.date)

                # print str(graph_data[0].date)
                # graph_data = str(graph_data)
                #chartit code
                #Step 1: Create a DataPool with the data we want to retrieve.
                weatherdata = \
                    DataPool(
                       series=
                        [{'options': {
                           'source': graph_data},
                          'terms': [
                            'completed_reps',
                            'total_reps',
                            'date']}
                         ])

                #Step 2: Create the Chart object
                cht = Chart(
                        datasource = weatherdata,
                        series_options =
                          [{'options':{
                              'type': 'line',
                              'stacking': False},
                            'terms':{
                              'date': [
                                'completed_reps',
                                'total_reps']
                              }}],
                        chart_options =
                          {'title': {
                               'text': label},
                           'xAxis': {
                                'title': {
                                   'text': 'Dates'}},
                            'yAxis':{
                                'title':{
                                    'text': 'Reps'
                                }
                            }       })

                #Step 3: Send the chart object to the template.
                # context_dict['weatherchart'] = cht
                # return render_to_response({'weatherchart': cht})

                #----------------------Time graph starts here
                   
                time_data = Results.objects.filter(patient=patient, exercise=exercise)

                # print str(graph_data[0].date)
                # graph_data = str(graph_data)
                #chartit code
                #Step 1: Create a DataPool with the data we want to retrieve.
                timedata = \
                    DataPool(
                       series=
                        [{'options': {
                           'source': time_data},
                          'terms': [
                            'completed_time',
                            'expected_time',
                            'date']}
                         ])

                #Step 2: Create the Chart object
                timecht = Chart(
                        datasource = timedata,
                        series_options =
                          [{'options':{
                              'type': 'line',
                              'stacking': False},
                            'terms':{
                              'date': [
                                'completed_time',
                                'expected_time']
                              }}],
                        chart_options =
                          {'title': {
                               'text': label},
                           'xAxis': {
                                'title': {
                                   'text': 'Dates'}},
                            'yAxis':{
                                'title':{
                                    'text': 'Time'
                                }
                            }       })

                #Step 3: Send the chart object to the template.
                context_dict['timechart'] = [cht, timecht]


                #----------------------Time graph ends here
                # Necessary stuff for context_dict
                results = Results.objects.filter(patient=patient)
                exercises = []
                exercise = []
                for i in results:
                    exercises.append(i.exercise)

                exercise = list(set(exercises))

                
                context_dict['results'] = results
                context_dict['patient'] = patient
                context_dict['exercise'] = exercise
                context_dict['yes'] = yes

                i_am_patient = 1
                context_dict['i_am_patient'] = i_am_patient

                
                return render(request, "results.html", context_dict)

@login_required
def user_logout(request):
    logout(request)
    # Redirect to a success page.
    return HttpResponseRedirect('/accounts/login/')

@user_passes_test(check_patient,redirect_field_name=None)
@login_required
def view_exercise(request, anystring=None):
    # exercise = Exercise.objects.get()
    # context_dict = {'exercise' : exercise}
    # return render(request, "exercise-list.html", context_dict)
    if anystring:
        try:
            exercise = Exercise.objects.get(exerciseName = anystring)
        # context_dict = {'exercise' : exercise}
            exercise_list = Exercise.objects.filter()
            context_dict = {'exercise': exercise, 'exercise_list' : exercise_list}
            return render(request, "exercise-list.html", context_dict)
        except:
            return HttpResponse("No exercise exists")

@user_passes_test(check_patient,redirect_field_name=None)
@login_required
def exercise(request):
    exercise_list = Exercise.objects.filter()
    context_dict = {'exercise_list' : exercise_list}
    return render(request, "exercise.html", context_dict)

@user_passes_test(check_patient,redirect_field_name=None)
@login_required
def add_exercise(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = ExerciseForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            f = form.save(commit=False)
            u = request.user
            therapist = Therapist.objects.get(username = u.username)
            f.added_by = therapist
            f.name = form.cleaned_data['exerciseName']
            f.category = form.cleaned_data['category']
            f.desc = form.cleaned_data['desc']
            f.gbd = form.cleaned_data['gbd_name']
            # f.date = form.cleaned_data['date']
            form.save(commit=True)
        #     exercise = Exercise.objects.get(exerciseName = f.name)
        # # context_dict = {'exercise' : exercise}
        #     exercise_list = Exercise.objects.filter()
        #     context_dict = {'exercise': exercise, 'exercise_list' : exercise_list}
            # return render(request, "exercise-list.html", context_dict)
            return upload_exercise(request)

    # if a GET (or any other method) we'll create a blank form
    else:
        form = ExerciseForm()

    return render(request, 'add_exercise.html', {'form': form})

@user_passes_test(check_patient,redirect_field_name=None)
@login_required
def view_patient(request):
    user = request.user
    therapist = Therapist.objects.filter(user=user)
    last_assigned_dates = []
    last_completed_dates = []
    try:
        patients = Patient.objects.filter(therapistId=therapist)
    
        for p in patients:
            sessions = Session.objects.filter(user=p).order_by('-sessdate')
            if sessions:
                a = datetime.date.today()
                if a>sessions[0].sessdate:
                    last_assigned_dates.append("0")
                else:
                    last_assigned_dates.append(sessions[0].sessdate)

                last_completed_dates.append("NA")
                for s in sessions:
                    if s.sessdate < a:
                        last_completed_dates[-1] = s.sessdate
                        # last_completed_dates.append(s.sessdate)
                        break

            if not sessions:
                last_assigned_dates.append("0")
                last_completed_dates.append("NA")
        print last_assigned_dates
        print last_completed_dates


        context_dict = {'patients' : patients, 'user':user, 'sessions':sessions,'last_assigned_dates':last_assigned_dates,
        'last_completed_dates':last_completed_dates,}
        return render(request, "patient_list.html", context_dict)
    except Exception, e:
        pass
    return create_new_patient_account(request)

    # print sessions
    # context_dict = {'patients' : patients, 'user':user, 'sessions':sessions,'last_assigned_dates':last_assigned_dates,}
    # return render(request, "patient_list.html", context_dict)

@user_passes_test(check_patient,redirect_field_name=None)
@login_required
def upload_exercise(request):
    # Handle file upload
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            newdoc = Document(docfile = request.FILES['docfile'])
            newdoc.save()

            # Redirect to the document list after POST
            
            return HttpResponseRedirect('/exercises/')
    else:
        form = DocumentForm() # A empty, unbound form

    # Load documents for the list page
    # documents = Document.objects.all()

    # Render list page with the documents and the form
    return render_to_response(
        'exercise-upload.html',
        {'form': form},
        context_instance=RequestContext(request)
    )

@user_passes_test(check_patient,redirect_field_name=None)
@login_required
def modify_exercise(request, anystring):
    # Handle file upload
    try:
        current = 'C:\\Users\\aakas\\Desktop\\Everything\\elemental_kinection'
        os.chdir(current)
        name = str(anystring)
        # current = os.getcwd()
        path = current + '\\kinect_app\\media\\gbd\\'
        print path
        os.chdir(path)
        os.remove(name)
        print path
        os.chdir(current)
        if request.method == 'POST':
            form = DocumentForm(request.POST, request.FILES)
            if form.is_valid():
                newdoc = Document(docfile = request.FILES['docfile'])
                # os.remove(name)
                newdoc.save() #---comment this out
                # os.chdir(current)

                # Redirect to the document list after POST
                
                return HttpResponseRedirect('/exercises/')
        else:
            form = DocumentForm() # A empty, unbound form

    except:
        if request.method == 'POST':
            form = DocumentForm(request.POST, request.FILES)
            if form.is_valid():
                newdoc = Document(docfile = request.FILES['docfile'])
                # os.remove(name)
                newdoc.save() #---comment this out
                # os.chdir(current)

                # Redirect to the document list after POST
                
                return HttpResponseRedirect('/exercises/')
        else:
            form = DocumentForm() # A empty, unbound form

    # Load documents for the list page
    # documents = Document.objects.all()

    # Render list page with the documents and the form
    return render_to_response(
        'exercise-upload.html',
        {'form': form},
        context_instance=RequestContext(request)
    )

@user_passes_test(check_patient,redirect_field_name=None)
@login_required
def patient_stats(request, anystring=None):
    if anystring:
            # user = request.user
            # therapist = Therapist.objects.filter(username=user.username)
            # group = models.Group.objects.get(name="Therapist")
            # users = group.user_set.all()
            
            patient = Patient.objects.get(username=anystring)
            results = Results.objects.filter(patient=patient)
            total_reps = []
            total = []
            completed_reps = []
            completed = []
            dates = []
            for result in results:
                dates.append(result.date)
                total_reps.append(result.total_reps)
                completed_reps.append(result.completed_reps)

            date = []
            month = []
            for d in dates:
                date.append(d.day)
                month.append(d.month)

            # for t in total_reps:
            #     total.append(int(t))

            # for c in completed_reps:
            #     completed.append(int(c))

            exercises = []
            exercise = []
            for i in results:
                exercises.append(i.exercise)

            exercise = list(set(exercises))

            # print total, completed, date
            context_dict = {'results' : results, 'patient' : patient, 'exercise' : exercise,}
            return render(request, "results.html", context_dict)

@user_passes_test(check_patient,redirect_field_name=None)
@login_required
def patient_exercise_stats(request, anynumber=None, anystring=None):
    if anynumber:
        if anystring:
            
                patient = Patient.objects.get(id = anynumber)
                exercise = Exercise.objects.filter(exerciseName = anystring)
                results = Results.objects.filter(patient=patient, exercise=exercise)
                yes = 1
                context_dict = {}
                label = str(anystring) + " Results"
                graph_data = Results.objects.filter(patient=patient, exercise=exercise)
                print graph_data[0].date
                print str(graph_data[0].date)
                for i in graph_data:
                    i.date = str(i.date)

                # print str(graph_data[0].date)
                # graph_data = str(graph_data)
                #chartit code
                #Step 1: Create a DataPool with the data we want to retrieve.
                weatherdata = \
                    DataPool(
                       series=
                        [{'options': {
                           'source': graph_data},
                          'terms': [
                            'completed_reps',
                            'total_reps',
                            'date']}
                         ])

                #Step 2: Create the Chart object
                cht = Chart(
                        datasource = weatherdata,
                        series_options =
                          [{'options':{
                              'type': 'line',
                              'stacking': False},
                            'terms':{
                              'date': [
                                'completed_reps',
                                'total_reps']
                              }}],
                        chart_options =
                          {'title': {
                               'text': label},
                           'xAxis': {
                                'title': {
                                   'text': 'Dates'}},
                            'yAxis':{
                                'title':{
                                    'text': 'Reps'
                                }
                            }       })

                #Step 3: Send the chart object to the template.
                # context_dict['weatherchart'] = cht
                # return render_to_response({'weatherchart': cht})

                #----------------------Time graph starts here
                   
                time_data = Results.objects.filter(patient=patient, exercise=exercise)

                # print str(graph_data[0].date)
                # graph_data = str(graph_data)
                #chartit code
                #Step 1: Create a DataPool with the data we want to retrieve.
                timedata = \
                    DataPool(
                       series=
                        [{'options': {
                           'source': time_data},
                          'terms': [
                            'completed_time',
                            'expected_time',
                            'date']}
                         ])

                #Step 2: Create the Chart object
                timecht = Chart(
                        datasource = timedata,
                        series_options =
                          [{'options':{
                              'type': 'line',
                              'stacking': False},
                            'terms':{
                              'date': [
                                'completed_time',
                                'expected_time']
                              }}],
                        chart_options =
                          {'title': {
                               'text': label},
                           'xAxis': {
                                'title': {
                                   'text': 'Dates'}},
                            'yAxis':{
                                'title':{
                                    'text': 'Time'
                                }
                            }       })

                #Step 3: Send the chart object to the template.
                context_dict['timechart'] = [cht, timecht]


                #----------------------Time graph ends here
                # Necessary stuff for context_dict
                results = Results.objects.filter(patient=patient)
                exercises = []
                exercise = []
                for i in results:
                    exercises.append(i.exercise)

                exercise = list(set(exercises))

                
                context_dict['results'] = results
                context_dict['patient'] = patient
                context_dict['exercise'] = exercise
                context_dict['yes'] = yes

                
                return render(request, "results.html", context_dict)

@user_passes_test(check_patient,redirect_field_name=None)
@login_required
def assign_session(request, anynumber, anystring):
    #need: exercises (by that therapist), a table for (exercise, reps, time) and session due date
    patient = Patient.objects.get(id = anynumber)
    exercises = Exercise.objects.all()
    sessions = Session.objects.filter(user=patient)
    session_by_date = []
    order = []
    date_object = datetime.datetime.now()
    url = '/session/assign/'+anynumber+'/'+anystring+'/'
    try:
        anystring = anystring.replace(".", "")
        date_object = datetime.datetime.strptime(anystring, '%b %d, %Y').date()
        date_object = date_object.strftime('%B %d, %Y')
        date_object = datetime.datetime.strptime(date_object, '%B %d, %Y').date()
        # date_object = date_object.strftime('%B %d, %Y')
    except Exception, e:
        pass
    try:
        date_object = datetime.datetime.strptime(anystring, '%B %d, %Y').date()
    except Exception, e:
        pass

    sessbydate = Session.objects.filter(sessdate=date_object, user=patient)
    for i in sessbydate:
        session_by_date.append(i.exercise)
        order.append(i.ordering)

    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = AssignSessionForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            
            f = form.save(commit=False)
            # f.ordering = form.cleaned_data['ordering']
            f.finished = 0
            f.sessdate = date_object
            f.exercise = form.cleaned_data['exercise']
            f.reps = form.cleaned_data['reps']
            f.time = form.cleaned_data['time']
            # f.user = form.cleaned_data['user']
            f.sequence = form.cleaned_data['sequence']
            f.user = patient
            f.completed_time = 0
            s = Session.objects.filter(user=patient, sessdate = f.sessdate).order_by('sessdate')
            ss = s[::-1]

            if int(ss[0].finished) == 0:
                if f.sequence == '3':
                    messages.error(request, "You must start a sequence first before using this option")
                    return HttpResponseRedirect(url)

            # print f.sequence
            if f.sequence == '0':
                f.finished = 0
            if f.sequence == '2':
                f.finished = 2
            if f.sequence == '3':
                f.finished = 3
            # f.finished = 0

            # print f.sequence
            # print f.finished
            

            form.save(commit=True)
            # print form
            url = '/session/assign/'+anynumber+'/'+anystring+'/'
            return HttpResponseRedirect(url)
            # return render(request, 'session_assign.html', {'form': form, 'patient': patient, 'sessions' : sessions,
            #     'sessbydate':sessbydate,'date_object':date_object,})
            # return upload_exercise(request)

    # if a GET (or any other method) we'll create a blank form
    else:
        form = AssignSessionForm()

    return render(request, 'session_assign.html', {'form': form, 'patient': patient, 'sessions' : sessions,
                'sessbydate':sessbydate,'date_object':date_object,})

@user_passes_test(check_patient,redirect_field_name=None)    
@login_required
def new_assign_session(request, anynumber):
    #need: exercises (by that therapist), a table for (exercise, reps, time) and session due date
    patient = Patient.objects.get(id = anynumber)
    exercises = Exercise.objects.all()
    sessions = Session.objects.filter(user=patient)

    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = NewAssignSessionForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            
            f = form.save(commit=False)
            # f.ordering = form.cleaned_data['ordering']
            f.sessdate = form.cleaned_data['sessdate']
            # d = datetime.datetime.strptime(str(f.sessdate), '%Y-%m-%d')
            # f.sessdate = datetime.date.strftime(d, "%B %d, %Y")
            f.exercise = form.cleaned_data['exercise']
            f.reps = form.cleaned_data['reps']
            f.time = form.cleaned_data['time']
            # f.user = form.cleaned_data['user']
            # f.user = patient
            f.finished = 0
            f.sequence = form.cleaned_data['sequence']
            f.user = patient
            f.completed_time = 0
            # print f.sequence
            if f.sequence == '0':
                f.finished = 0
            if f.sequence == '2':
                f.finished = 2
            
            form.save(commit=True)
            # print form
            anystring = f.sessdate
            anystring = anystring.strftime('%B %d, %Y')
            try:
                anystring = anystring.replace(".", "")
                date_object = datetime.datetime.strptime(anystring, '%b %d, %Y').date()
                date_object = date_object.strftime('%B %d, %Y')
                date_object = datetime.datetime.strptime(date_object, '%B %d, %Y').date()
                # date_object = date_object.strftime('%B %d, %Y')
            except Exception, e:
                pass
            try:
                date_object = datetime.datetime.strptime(anystring, '%B %d, %Y').date()
            except Exception, e:
                pass
            url = '/session/assign/'+anynumber+'/'+anystring+'/'
            return HttpResponseRedirect(url)
            # return render(request, 'session_assign.html', {'form': form, 'patient': patient, 'sessions' : sessions,
            #     'sessbydate':sessbydate,'date_object':date_object,})
            # return upload_exercise(request)

    # if a GET (or any other method) we'll create a blank form
    else:
        form = NewAssignSessionForm()

    return render(request, 'new_session_assign.html', {'form': form, 'patient': patient, 'sessions' : sessions,})


@user_passes_test(check_patient,redirect_field_name=None)    
@login_required
def session(request, anynumber):

    patient = Patient.objects.get(id = anynumber)
    exercises = Exercise.objects.all()
    sessions = Session.objects.filter(user = patient)
    dates_ordering = Session.objects.filter(user=patient).order_by('-sessdate')
    dates = []
    # session = []
    for i in dates_ordering:
        dates.append(i.sessdate)
        # session.append(i.exercise)
    

    dates = list(set(dates))
    dates.sort( reverse=True)
    return render(request, 'session.html', {'sessions' : sessions, 'patient' : patient, 'dates' : dates,})

    # return HttpResponse("Oh hello!")

from django.conf.urls import patterns, url
from kinect_app import views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = patterns('kinect_app',
        url(r'^$', views.index, name='index'),
        # url(r'^accounts/new/', views.create_new_account, name='new_account'),
        # url(r'^accounts/new-patient/', views.create_new_patient_account, name='new_patient_account'),
        # url(r'^accounts/login/$','login',{'template_name':'login.html'}),
        url(r'^welcome/','views.welcome_user', name='welcome'),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
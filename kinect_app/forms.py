from .models import Therapist, Patient, AppAdmin, Exercise, Session
from django import forms
from django.forms import ModelForm,Textarea
from django.utils.translation import ugettext_lazy as _

class DateInput(forms.DateInput):
    input_type = 'date'

class TherapistForm(forms.ModelForm):
    class Meta:
        model = Therapist
        fields = ['username', 'password', 'firstname', 'lastname', 'workplace']
        labels = {
            'firstname': _('First Name'),
            'lastname': _('Last Name'),
            'workplace': _('Workplace'),
        }
        widgets = {
        'password': forms.PasswordInput(),
        }

class AdminForm(forms.ModelForm):
    class Meta:
        model = AppAdmin
        fields = ['username', 'password', 'firstname', 'lastname',]
        widgets = {
        'password': forms.PasswordInput(),
        }

class PatientForm(forms.ModelForm):

    confirm = forms.CharField(label=_(u'Confirm Password'), required=True, widget=forms.PasswordInput)
    class Meta:
		model = Patient
		fields = ['username', 'password', 'confirm','start_date', 'end_date']
		widgets = {
		'password': forms.PasswordInput(),
        'confirm': forms.PasswordInput(),
        'start_date': DateInput(),
        'end_date': DateInput(),
		}


class ExerciseForm(forms.ModelForm):
    class Meta:
        model = Exercise
        fields = ['exerciseName','category','desc','gbd_name']
        labels = {
            'exerciseName': _('Exercise Name'),
            'desc': _('Description'),
        }
        help_texts = {
            'desc': _('A brief description for patient on how to perform the exercise')
        }
        # docfile = forms.FileField(
        # label='Select a file')

class DocumentForm(forms.Form):
    docfile = forms.FileField(
        label='Select a file',
        help_text='max. 10 megabytes'
    )

Sequence = (  
    ('0', 'Not in sequence'),
    ('2', '1st exercise of a sequence'),
    ('3', 'Part of a sequence'),
)

NewSequence = (  
    ('0', 'Not in sequence'),
    ('2', '1st exercise of a sequence'),
)


class AssignSessionForm(forms.ModelForm):

    # sessdate = forms.DateField(widget=forms.TextInput(attrs={'id':'datepicker'}))
    exercise = forms.ModelChoiceField(queryset=Exercise.objects.order_by('exerciseName'))
    sequence = forms.ChoiceField(choices=Sequence, required=True )
    class Meta:
        model = Session
        fields = ['exercise','reps', 'time','sequence',]
        labels = {
            # 'sessdate': _('Session due date'),
            'reps': _('Total reps'),
            'time': _('Estimated time to complete (sec.)'),
            # 'user': _('Patient to assign'),
            'ordering': _('Order of this exercise'),
        }
        help_texts = {
            # 'sessdate': _('Date on which this session is due.'),
            # 'time': _('in seconds.'),
            'ordering': _('1 - any order'),
        }
        widgets = {
            # 'sessdate': DateInput(),
            # 'reps': Textarea(attrs={'cols': 80, 'rows': 1}),
        }

class NewAssignSessionForm(forms.ModelForm):

    # sessdate = forms.DateField(widget=forms.TextInput(attrs={'id':'datepicker'}))
    exercise = forms.ModelChoiceField(queryset=Exercise.objects.order_by('exerciseName'))
    sequence = forms.ChoiceField(choices=NewSequence, required=True )
    class Meta:
        model = Session
        fields = ['sessdate','exercise','reps', 'time', 'sequence',]
        labels = {
            'sessdate': _('Session due date'),
            'reps': _('Total reps'),
            'time': _('Estimated time to complete (sec.)'),
            # 'user': _('Patient to assign'),
            'ordering': _('Order of this exercise'),
        }
        help_texts = {
            'sessdate': _('Date on which this session is due. (MM/DD/YYYY)'),
            # 'time': _('in seconds.'),
            'ordering': _('1 - any order'),
        }
        widgets = {
            'sessdate': DateInput(),
            # 'reps': Textarea(attrs={'cols': 80, 'rows': 1}),
        }



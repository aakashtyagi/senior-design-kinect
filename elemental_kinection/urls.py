from django.conf.urls import patterns, include, url
from django.contrib import admin
from kinect_app import views

urlpatterns = patterns('django.contrib.auth.views',
    # Examples:
    # url(r'^$', 'elemental_kinection.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^kinect-app/', include('kinect_app.urls')),
    url(r'^exercises/$', views.exercise, name='exercise'),
    url(r'^exercises/(?P<anystring>.+)/$', views.view_exercise, name='exercise_list'),

    url(r'^patient-stats/(?P<anystring>.+)/$', views.patient_stats, name='stats'),
    url(r'^patient-exercise-stats/(?P<anynumber>\d+)/(?P<anystring>.+)/$', views.patient_exercise_stats, name='exercise_stats'),

    url(r'^patient/(?P<anystring>.+)/$', views.for_patient_stats, name='for_stats'),
    url(r'^patient-exercise/(?P<anynumber>\d+)/(?P<anystring>.+)/$', views.for_patient_exercise_stats, name='for_exercise_stats'),

    url(r'^session/assign/(?P<anynumber>\d+)/(?P<anystring>.+)/$', views.assign_session, name='assign_session'),
    url(r'^new-session/assign/(?P<anynumber>\d+)/$', views.new_assign_session, name='new_assign_session'),
    url(r'^session/(?P<anynumber>\d+)/$', views.session, name='session'),

    url(r'^accounts/new/', views.create_new_therapist_account, name='new_account'),
    url(r'^accounts/new-patient/', views.create_new_patient_account, name='new_patient_account'),
    url(r'^accounts/new-admin/', views.create_new_admin_account, name='new_admin_account'),
    url(r'^accounts/login/$',views.user_login, name='login'),
    url(r'^$',views.user_login, name='login'),
    url(r'^accounts/logout/$', views.user_logout, name='logout'),
    url(r'^add-exercise/$', views.add_exercise, name='add_exercise'),
    url(r'^patient-list/$', views.view_patient, name='view_patient'),
    #   
    ## yo yo    
    url(r'^exercise-upload/$', views.upload_exercise, name='upload'),
    url(r'^exercise-modify/(?P<anystring>.+)/$', views.modify_exercise, name='modify'),
)
